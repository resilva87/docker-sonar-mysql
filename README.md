# docker-sonar-mysql
Docker-compose with [SonarQube](http://www.sonarqube.org/) backed by a [MySQL](https://www.mysql.com/) database.

## ComposeHub

You can check the application directly from https://composehub.com/package/docker-sonar-mysql

## Using
With [docker-compose](https://docs.docker.com/compose/) installed, simply `git clone` this repo, run `docker-compose build` and `docker-compose up`.

Exposed ports:

+ 19000 => SonarQube Web
+ 19306 => MySQL connection

Volumes (same name in container and host, change them if needed):

+ /opt/sonarqube/conf => Maps to SonarQube configuration folder
+ /opt/sonarqube/extensions => Maps to SonarQube plugins and extensions folder
+ /opt/sonarqube/data => Maps to MySQL data folder