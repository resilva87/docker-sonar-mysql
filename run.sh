#!/usr/bin/env bash
set -e

# wait mysql container to be up and running
# http://stackoverflow.com/questions/31746182/docker-compose-wait-for-container-x-before-starting-y
while ! nc -z db 3306; do sleep 3; done

if [ "${1:0:1}" != '-' ]; then
	exec "$@"
fi

exec java -jar "lib/sonar-application-$SONAR_VERSION.jar" \
-Dsonar.log.console=true \
-Dsonar.jdbc.username="$MYSQL_USER" \
-Dsonar.jdbc.password="$MYSQL_PASSWORD" \
-Dsonar.jdbc.url="$SONARQUBE_JDBC_URL"  \
"$@"
